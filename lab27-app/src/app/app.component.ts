import { Component } from '@angular/core';
import { DonneesService, PersonneInfo } from './donnees.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lab27-app';
  // ds = new DonneesService(null);
  persrecords: PersonneInfo[] = [];
  persorecords: PersonneInfo[] = []
  updatetime: string = '';
  constructor(public ds: DonneesService) { }
  getall(): any[] {

    return this.ds.getelems();

  }

  updatedate(): void {

    let targeturl = 'http://localhost:3000/getjson?f=etudiants.json';
    this.persrecords = this.ds.getrecords(targeturl);
    this.persrecords = this.ds.personnes;
    let now = new Date();
    this.updatetime = ""+now.getFullYear() + "-"+ now.getMonth() + '-' + now.getDate()+"";
    this.updatetime=this.updatetime+"  " + now.getHours() + ':'+now.getMinutes()+':'+now.getSeconds();
  }

}
