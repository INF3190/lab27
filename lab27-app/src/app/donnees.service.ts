import { async, waitForAsync, TestBed } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Observable} from "rxjs";
import { map, delay } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class DonneesService {
  private elems: any[] = [{ "nom": "Charles", "prenom": "Georges" }, { "nom": "Marcel", "prenom": "Albert" }];
  personnes: PersonneInfo[] = [];

  constructor(protected http: HttpClient) { }

  getelems(): any[]{
    return this.elems;
  }

  getrecords(targeturl: string): PersonneInfo[]{
    let returnval = [];
    //this.callhttpget(targeturl);
    this.callhttpgetpromise(targeturl).then((requesteddata) => { this.personnes = requesteddata; });
    return this.personnes;
  }

  /*
  * Appel de la methode http on utilise async pour utiliser wait afin d'attendre la réponse
  */
  async callhttpget(targeturl: string):Promise<any[]> {

    //let requesteddata = await this.http.get<PersonInfo[]>(targeturl).subscribe(data => this.elems = data);

    let requesteddata = await this.http.get<PersonneInfo[]>(targeturl, { "responseType": "json" }).subscribe(
      (response) => { this.personnes = response;  },
      (error) => { console.log(error); }
    );
    console.log(this.elems);
    return (await this.personnes);
  }

  async callhttpgetpromise(targeturl: string): Promise<PersonneInfo[]>{
    let requesteddata:PersonneInfo[] = await this.http.get<PersonneInfo[]>(targeturl, { "responseType": "json" }).toPromise();
    console.log("callhttpgetpromise");
    console.log(requesteddata);
    //requesteddata.then((requesteddata) => { this.personnes = requesteddata; });
    return (await requesteddata);

  }



}

/*
* interface format de données
*/
export interface PersonneInfo{
  nom: string;
  prenom: string;
  age: Number;
}
